from flask import Flask,render_template,request
from flaskext.mysql import MySQL
import mysql.connector
from flask_cors import CORS

app = Flask(__name__)


db_config = {
    'host': 'localhostrg',
    'user': 'rootdg',
    'password': 'athulYA',
    'database': 'ath_clients'
}

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/submit',methods=['POST'])
def submit():
    if request.method == 'POST':
        firstName = request.form.get('firstName')
        lastName = request.form.get('lastName')
        age = request.form.get('age')
        gender = request.form.get('gender')
        ph  = request.form.get('ph')
        services = ', '.join(request.form.getlist('service'))

        try:

            conn = mysql.connector.connect(**db_config)
            cursor = conn.cursor()
            insert_query = "Insert into clientdetails (firstname,lastname, age, gender, ph, services) values (%s,%s,%s,%s,%s,%s)"
            cursor.execute(insert_query,(firstName,lastName,age,gender,ph,services))
            conn.commit()
            conn.close()
            return 'Form Submitted successfully!'
        except mysql.connector.Error as e:
            return  e
            @app.route('/clients', methods=['GET'])
            def get_clients():
                try:
                    conn = mysql.connector.connect(**db_config)
                    cursor = conn.cursor()
                    select_query = "SELECT * FROM clientdetails"
                    cursor.execute(select_query)
                    rows = cursor.fetchall()
                    conn.close()
                    return {'clients': rows}
                except mysql.connector.Error as e:
                    return str(e)
if __name__ == '__main__':
    app.run(debug=True)

    @app.route('/random', methods=['GET'])
    def random_route():
        return "This is a random route!"